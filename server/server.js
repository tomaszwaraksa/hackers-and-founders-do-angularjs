// Load express module
var express = require("express");

// initialize the express application
var app = express();

// instruct it to serve static files from ../source directory
// where our Angular Tutorial application is found
var appFolder = __dirname + "/../source";
app.use(express.static(appFolder));

// and listen on a given port
var port = 1234;
app.listen(port, function() {    
    console.log("Angular Tutorial");    
    console.log("- serving files from " + appFolder);
    console.log("- listening at http://localhost:" + port);
    console.log("Enjoy!"); 
});