var http = require("http");
var port = 1234;


function requestHandler(request, response) {
    var description = request.method + ' ' + request.url;
    console.log(description);
    response.end(description + ' requested');
}


http
    .createServer(requestHandler)
    .listen(port, function() {
       console.log('Angular Tutorial available at http://localhost:' + port + ', enjoy!'); 
    });