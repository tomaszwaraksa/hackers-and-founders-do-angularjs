angular
    .module("NewsApp")
    .factory("UserService", function($q) {
        var service = {
            name: null,
            loggedIn: false,
            
            logIn: function(name, password) {
                service.logOut();
                if (name && password && name == "tomasz" && password == "123") {
                    service.loggedIn = true;
                    service.name = name;                    
                }
                return $q.when(service.loggedIn);
            },
            
            logOut: function() {                
                service.loggedIn = false;
                service.name = null;
            }
        };
    
        return service;
    });