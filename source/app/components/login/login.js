angular.module("NewsApp")
    .component("newsAppLogin", {  
        templateUrl: "/app/components/login/login.html",
    
        controller: function($state, UserService) {
            var ctrl = this;
            
            ctrl.username = "tomasz";
            ctrl.password = "123"; // not nice! ... but saves some typing during demo ;-)
            
            ctrl.cancel = function() {
                $state.go("home");
            };
            
            ctrl.ok = function() {
                UserService
                    .logIn(ctrl.username, ctrl.password)
                    .then(function() {
                        if (UserService.loggedIn)
                            $state.go("home");
                    });
            };
        }
    
    });


