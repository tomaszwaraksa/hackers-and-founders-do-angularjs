angular.module("NewsApp")
    .component("newsAppHeader", {  
        templateUrl: "/app/components/header/header.html",
    
        controller: function($state, UserService) {
            var ctrl = this;
            ctrl.UserService = UserService;
            ctrl.logOut = function() {
                UserService.logOut();
                $state.go("home");
            }
        }
    
    });


