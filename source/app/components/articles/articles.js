angular.module("NewsApp")
    .component("newsAppArticles", {  
        templateUrl: "/app/components/articles/articles.html",
    
        controller: function($state, $interval, ArticlesService, UserService) {
            var ctrl = this;   
            
            // expose the service to databindings in HTML 
            ctrl.ArticlesService = ArticlesService;
            ctrl.UserService = UserService;
            
            ctrl.count = function() {
                return ArticlesService.items.length; 
            };

            ctrl.addArticle = function() {
                if (UserService.loggedIn) 
                    $state.go('add');
            }
            
            if (!ArticlesService.populated)
                ArticlesService.load();
            
        }
    
    });


