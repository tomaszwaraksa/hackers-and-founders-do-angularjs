angular
    .module("NewsApp")
    .factory("ArticlesService", function($http, $log, $timeout) {
    
        var service = {
            
            populating: false,
            populated: false,
            items: [],
            
            // Loads all articles from the server
            load: function() {
                service.populating = true;
                $timeout(function() {                    
                    $http
                        .get("app/data/articles.json", { cache: false })
                        .then(function(response)
                        {
                            $log.info("Got the data from the server", response);
                            service.items = response.data;
                            service.populated = true;
                        })
                        .finally(function() {
                            service.populating = false;
                        });
                }, 2000);
            },

            // Creates a new article (without adding to the collection yet!)
            create: function() {
                return {                    
                    "id": null,
                    "date": service.now(),
                    "title": "New article",
                    "intro": "Brief introduction ...",
                    "content": null,
                    "visible": true,
                    "full": false
                }
            },

            // Returns an article with the specified identifier
            get: function(id) {
                return _.filter(service.items, function(item) { return item.id == id; })[0];
            },
            
            // Adds an article to the collection
            add: function(article) {
                if (article) {
                    // assign id
                    article.id = _.max(service.items, function(item) { return item.id; }) + 1;
                    service.items.push(article);
                    // now a call to REST endpoint should follow, to actually save it ...
                    // ... leaving this to you :-)
                }
            },

            // Deletes the specified article
            delete: function(article) {
                if (article)
                    _.remove(service.items, function(item) {
                        return item.id == article.id;
                    })
            },

            // Returns current date formatted 
            now: function() {
                var s = (new Date()).toISOString();
                return s.substr(0, 4) + '/' + s.substr(5, 2) + s.substr(8, 2);
            },
            
            // Validates the article
            validate: function(article) {
                var result = {
                    ok: true,
                    errors: []
                };
                if (!article)
                    result.errors.push('Article cannot be empty or null');
                else {
                    if (!article.title || article.title.toString().trim() == "")
                        result.errors.push('Article title not specified');
                    if (!article.intro || article.intro.toString().trim() == "")
                        result.errors.push('Article introduction not specified');
                    if (!article.content || article.content.toString().trim() == "")
                        result.errors.push('Article content not specified');
                }
                result.ok = result.errors.length == 0;
                return result;
            }
        };
      
        console.log("ArticlesService is ready.");
         
        return service;
    });