angular.module("NewsApp")
    .component("newsAppArticleEdit", {  
        templateUrl: "/app/components/articles/article-edit.html",
    
        controller: function($state, $stateParams, ArticlesService) {
            // Parameters from route URL are available in $stateParams 
            // If article id is there, we're editing an existing article,
            // otherwise we're adding a new one
            var ctrl = this;
            ctrl.errors = [];
            ctrl.article = ArticlesService.create();
            ctrl.dialogTitle = "New Article";
            
            
            // try fetch the article 
            var articleId = $stateParams.id;
            if (articleId) {
                ctrl.current = ArticlesService.get(articleId);
                // Copy data to the local article.
                // We're editing a local copy instead of data binding 
                // to the original record, so that it stays intact in case of cancel.
                if (ctrl.current) { 
                    _.assign(ctrl.article, ctrl.current);
                    ctrl.dialogTitle = "Article #" + ctrl.article.id;
                }
            }
            

            ctrl.cancel = function() {
                $state.go('home');
            }


            ctrl.save = function() {
                ctrl.errors = []; 
                var validation = ArticlesService.validate(ctrl.article);
                if (validation.ok) {
                    // if editing an existing article, just update,
                    // otherwise add to collection of articles
                    if (ctrl.current)
                        _.assign(ctrl.current, ctrl.article);
                    else
                        ArticlesService.add(ctrl.article);
                    $state.go('home');
                }
                else {
                    ctrl.errors = validation.errors;
                }
            }
        }

    
    });


