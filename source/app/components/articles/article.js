angular.module("NewsApp")
    .component("newsAppArticle", {  
        templateUrl: "/app/components/articles/article.html",
    
        bindings: {
          article: "="  
        },
    
        controller: function($state, ArticlesService, UserService) {
            // whatever defined in bindings above, is already available 
            // through this.***
            var ctrl = this;
            
            ctrl.UserService = UserService;

            ctrl.delete = function() {
                ArticlesService.delete(ctrl.article);
            }

            ctrl.edit = function() {
                $state.go('edit', { id: ctrl.article.id });
            }
        }

    
    });


